// SPDX-License-Identifier: MIT-0

#include <iostream>
#include <random>
#include <vector>
#include <chrono>
#include <memory>
#include <algorithm>
#include <thread>
#include <mutex>
#include <future>
#include <sstream>
#include <cmath>
#include <NTL/ZZ.h>
#include <NTL/RR.h>
#include <NTL/matrix.h>
#include <NTL/LLL.h>
#include "acd.hxx"
#include "factor.hxx"
#include "result.hxx"
#include "misc.hxx"

using namespace std;
using namespace NTL;

struct Config {
    const long lambda;
    const long plus_t;    // dim = floor(gamma/eta) + plus_t + 1
    const long nattempts;
    const long da; // numerator of delta in the LLL algorithm
    const long db; // denominator of delta
    const unsigned nths; // # of threads

    Config(long lambda, long plus_t, long nattempts,
           long da, long db, long nths):
        lambda(lambda), plus_t(plus_t), nattempts(nattempts),
        da(da), db(db), nths(nths) {}
};

struct SdaResult: public Result {
    long heu_cnt;
    long nonzero;
    long success;

    SdaResult(long dim):
        Result(dim),  heu_cnt(0), nonzero(0), success(0) {}
    using Result::merge;
    void merge(const SdaResult &r) {
        Result::merge(r);
        heu_cnt += r.heu_cnt;
        nonzero += r.nonzero;
        success += r.success;
    }
};

static mutex cout_mtx; // mutex for std::cout

static Mat<ZZ> basis_matrix(const vector<ZZ> &x, long t, long rho)
{
    Mat<ZZ> B;
    B.SetDims(t + 1, t + 1);
    B[0][0] = power2_ZZ(rho + 1);
    for (long i = 1; i <= t; i++) {
        B[0][i] = x[i];
        B[i][i] = -x[0];
    }
    return B;
 }

static bool heuristic(Acd &a, long t)
{
    long rho = a.rho();
    long eta = a.eta();
    long gamma = a.gamma();

    // l = 0.47 * (1.04)^{t + 1} * 2^{gamma + rho - eta}
    RR l{1.04};
    l = power(l, t + 1);
    l *= to_RR(power2_ZZ(gamma + rho - eta));
    l *= to_RR(0.47);

    // n = rho + 1 + gamma * t
    ZZ n{gamma};
    n *= t;
    n += rho + 1;
    RR b{2};
    // r = 2^{n/(t + 1)}
    RR r{pow(b, to_RR(n) / (t + 1))};
    // y = sqrt(2 * pi * e)
    RR y{sqrt(b * ComputePi_RR() * exp(to_RR(1)))};
    // r = 2^{(rho + 1 + gamma * t)/(t + 1)}/sqrt(2 * pi * e)
    r /= y;

    return l < r;
}

static bool check_candidate(const ZZ &p, const vector<ZZ> &x, const Acd &a)
{
    ZZ pow2_rho{power2_ZZ(a.rho())};
    for (const auto &e: x) {
        ZZ r{e % p};
        if (r >= pow2_rho)
            if (p * (e / p + 1) - e >= pow2_rho)
                return false;
    }
    return true;
}

static SdaResult sda_experiment(unsigned id, const Config &c)
{
    auto a = Acd(c.lambda);
    const long m = a.gamma() / a.eta() + 1;
    const long t = m + c.plus_t;
    const long d = t + 1; // dim
    ZZ pow2_rho{power2_ZZ(a.rho())};
    auto res = SdaResult(d);
    stringstream ss;
    for (auto i = id + 1; i <= c.nattempts; i += c.nths) {
        a.reset();
        vector<ZZ> x;
        for (long j = 0; j < d; ++j)
            x.emplace_back(a.sample());
        Mat<ZZ> B{basis_matrix(x, t, a.rho())};
        ZZ det2;
        ss << i << "/" << c.nattempts << ": ";
        string now{ss.str()};
        ss_reset(ss);
        ss << now << "starts (dim = " << m << " + "
           << c.plus_t << " + 1 = " << d << ")" << endl;
        th_log(id, ss, cout_mtx);
        auto start = chrono::system_clock::now();
        LLL(det2, B, c.da, c.db);
        auto end = chrono::system_clock::now();
        double elapsed = chrono::duration_cast<chrono::seconds>(end-start).count();
        ss << now << "elapsed time [s] = "
           << elapsed << endl;
        th_log(id, ss, cout_mtx);
        if (heuristic(a, t)) {
            res.heu_cnt++;
            ss << now << "heu_cnt++" << endl;
            th_log(id, ss, cout_mtx);
            if (B[0][0] != 0) {
                res.nonzero++;
                ss << now << "nonzero++" << endl;
                th_log(id, ss, cout_mtx);
                ZZ q{B[0][0] / power2_ZZ(a.rho() + 1)};
                ZZ r{x[0] % q};
                if (r >= pow2_rho)
                    r = x[0] - q * (x[0] / q + 1);
                if (abs(r) < pow2_rho) {
                    ZZ p{abs((x[0] - r) / q)};
                    if (check_candidate(p, x, a)) {
                        res.success++;
                        ss << now << "success++: " << endl;
                        th_log(id, ss, cout_mtx);
                        ss << now << "true p  = " << a.p() << endl;
                        th_log(id, ss, cout_mtx);
                        ss << now << "cand. p = " << p << endl;
                        th_log(id, ss, cout_mtx);
                    }
                }
            }
        }
        auto f = Factor(B[0], d, det2);
        res.add(f, elapsed);
        ss << now << "f1 = " << f.factor1 << endl;
        th_log(id, ss, cout_mtx);
        ss << now << "f2 = " << f.factor2 << endl;
        th_log(id, ss, cout_mtx);
        ss << now << "f8 = " << f.factor8 << endl;
        th_log(id, ss, cout_mtx);
    }
    return res;
}

int main(int argc, char **argv)
{
    if (argc != 7) {
        cout << "usage: ./a.out lambda plus_t nattempts a b nthreads" << endl;
        exit(0);
    }
    long lambda = strtol(argv[1], NULL, 10);
    long plus_t = strtol(argv[2], NULL, 10);
    long nattempts = strtol(argv[3], NULL, 10);
    long da = strtol(argv[4], NULL, 10);
    long db = strtol(argv[5], NULL, 10);
    if (!da || !db) {
        // default values
        da = 3;
        db = 4;
    }
    long tmp = strtol(argv[6], NULL, 10);
    const unsigned nths =
        (1 <= tmp && tmp <= thread::hardware_concurrency())
        ? static_cast<unsigned>(tmp) : thread::hardware_concurrency();

    std::mt19937 rand_src(random_device{}());
    ZZ seed{static_cast<long>(rand_src())};
    SetSeed(seed);

    auto c = Config(lambda, plus_t, nattempts, da, db, nths);
    vector<future<SdaResult>> f_results;
    for (unsigned i = 0; i < nths; ++i)
        f_results.emplace_back(async(launch::async, sda_experiment, i, c));
    vector<SdaResult> results;
    for (auto &e: f_results)
        results.emplace_back(e.get());
    auto r = SdaResult(results[0].dim);
    for (const auto &e: results)
        r.merge(e);

    auto a = Acd(c.lambda);
    cout << endl;
    cout << "Settings:" << endl;
    cout << "lambda = " << c.lambda << endl;
    cout << "plus_t = " << c.plus_t << endl;
    cout << "nattempts = " << c.nattempts << endl;
    cout << "da = " << c.da << endl;
    cout << "db = " << c.db << endl;
    cout << "nths = " << c.nths << endl;
    cout << "rho = " << a.rho() << endl;
    cout << "eta = " << a.eta() << endl;
    cout << "gamma = " << a.gamma() << endl;
    cout << "dim = " << r.dim - plus_t - 1
         << " + plus_t + 1 = " << r.dim << endl;
    cout << endl;
    cout << "Result (mean):" << endl;
    cout << "f1 = " << r.mean1() << endl;
    cout << "f2 = " << r.mean2() << endl;
    cout << "f8 = " << r.mean8() << endl;
    cout << "elapsed_time [s] = " << r.mean_elapsed() << endl;
    cout << "heu_cnt = " << r.heu_cnt << endl;
    cout << "nonzero = " << r.nonzero << endl;
    cout << "success = " << r.success << endl;

    return 0;
}
