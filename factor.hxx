// SPDX-License-Identifier: MIT-0

#pragma once

#include <memory>
#include <NTL/mat_ZZ.h>
#include <NTL/RR.h>
#include "norm.hxx"

// factor* = (||b_1||_*/det(L)^{1/d})^{1/d}
struct Factor {
    const NTL::Vec<NTL::ZZ> v;
    const std::unique_ptr<Norm> n1, n2, n8;
    const long d;
    const NTL::RR det, dinv, pow_det_dinv;
    const NTL::RR norm1, norm2, norm8;
    const NTL::RR factor1, factor2, factor8;

    Factor(const NTL::Vec<NTL::ZZ> &v, long dim, NTL::ZZ det2):
        v(v), n1(norm(NORM1)), n2(norm(NORM2)), n8(norm(NORM8)),
        d(dim), dinv(NTL::inv(NTL::to_RR(dim))),
        pow_det_dinv(NTL::pow(NTL::sqrt(NTL::to_RR(det2)), dinv)),
        factor1(pow(n1->norm(v)/pow_det_dinv, dinv)),
        factor2(pow(n2->norm(v)/pow_det_dinv, dinv)),
        factor8(pow(n8->norm(v)/pow_det_dinv, dinv))
    {
    }
};
