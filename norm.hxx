// SPDX-License-Identifier: MIT-0

#pragma once

#include <memory>
#include <NTL/vector.h>
#include <NTL/ZZ.h>
#include <NTL/RR.h>

enum NormType {
    NORM1, // Absolute-value norm
    NORM2, // Euclidean norm
    NORM8  // Maximum norm
};

class Norm {
public:
    virtual ~Norm() {}
    virtual NTL::RR norm(const NTL::Vec<NTL::ZZ> &x) const = 0;
};

class Norm1: public Norm {
public:
    NTL::RR norm(const NTL::Vec<NTL::ZZ> &x) const override {
        NTL::ZZ sum{0};
        for (const auto &e: x)
            sum += NTL::abs(e);
        return NTL::to_RR(sum);
    }
};

class Norm2: public Norm {
public:
    NTL::RR norm(const NTL::Vec<NTL::ZZ> &x) const override {
        NTL::ZZ ip;
        NTL::InnerProduct(ip, x, x);
        return NTL::sqrt(NTL::to_RR(ip));
    }
};

class Norm8: public Norm {
public:
    NTL::RR norm(const NTL::Vec<NTL::ZZ> &x) const override {
        NTL::ZZ m{0};
        for (const auto &e: x) {
            NTL::ZZ f = NTL::abs(e);
            if (f > m)
                m = f;
        }
        return NTL::to_RR(m);
    }
};

static std::unique_ptr<Norm> norm(NormType type)
{
    switch (type) {
    case NORM1:
        return std::unique_ptr<Norm>(new Norm1());
    case NORM2:
        return std::unique_ptr<Norm>(new Norm2());
    case NORM8:
        return std::unique_ptr<Norm>(new Norm8());
    }
}

