// SPDX-License-Identifier: MIT-0

#include "acd.hxx"

Acd::Acd(long lambda)
{
    if (lambda < 2)
        set_params(2);
    else
        set_params(lambda);
    _p = gen_seckey();
    set_vars();
}

/*
 * (rho, eta, gamma) = (lmd, lmd^2, lmd^5)
 * If you want to change this setting,
 * define a subclass and override this function.
 */
void Acd::set_params(long lambda)
{
    _lambda = lambda;
    _rho = _lambda;
    _eta = _lambda * _lambda;
    _gamma = _eta * _eta * _lambda;
}

void Acd::set_vars()
{
    _pow2_gamma_over_p = NTL::power2_ZZ(_gamma) / _p;
    _pow2_rho = NTL::power2_ZZ(_rho);
    _pow2_rho_p1_m1 = 2 * _pow2_rho - 1;
}

void Acd::reset(long lambda)
{
    if (lambda < 2)
        set_params(_lambda);
    else
        set_params(lambda);
    _p = gen_seckey();
    set_vars();
}

NTL::ZZ Acd::gen_seckey()
{
    NTL::ZZ p{NTL::RandomLen_ZZ(_eta)};
    while (p % 2 == 0)
        NTL::RandomLen(p, _eta);
    return p;
}

NTL::ZZ Acd::sample(bool exact_multiple) const
{
    NTL::ZZ q{NTL::RandomBnd(_pow2_gamma_over_p)};
    if (exact_multiple)
        return _p * q;
    NTL::ZZ r{NTL::RandomBnd(_pow2_rho_p1_m1)};
    r -= _pow2_rho - 1;
    return _p * q + r;
}
