CXX := clang++
CXXFLAGS := -std=c++17 -Wall -Wextra -march=native -O2
LIBS := -lntl -lgmp -lpthread
INCLUDE := -I${HOME}/.local/include
LDFLAGS := -L${HOME}/.local/lib

TARGETS := avg sda
AVG_SRCS := avg.cxx
AVG_OBJS := $(AVG_SRCS:%.cxx=%.o)
SDA_SRCS := acd.cxx sda.cxx
SDA_OBJS := $(SDA_SRCS:%.cxx=%.o)
SRCS := $(AVG_SRCS) $(SDA_SRCS)
OBJS := $(AVG_OBJS) $(SDA_OBJS)
DEPS := $(SRCS:%.cxx=%.d)

all: $(TARGETS)

sda: $(SDA_OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS)

avg: $(AVG_OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS)

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -MMD -MP -o $@ -c $<

.PHONY: clean all

clean:
	-rm -rf $(OBJS) $(DEPS)

-include $(DEPS)
