// SPDX-License-Identifier: MIT-0

#include <iostream>
#include <sstream>
#include <random>
#include <vector>
#include <chrono>
#include <memory>
#include <string>
#include <mutex>
#include <thread>
#include <future>
#include <NTL/LLL.h>
#include "factor.hxx"
#include "result.hxx"
#include "randommatrix.hxx"
#include "misc.hxx"

using namespace std;
using namespace NTL;

struct Config {
    const RandomMatrixType type;
    const long dim; // dimension of basis matrix (# of rows)
    const long nattempts;
    const long da; // numerator of delta in the LLL algorithm
    const long db; // denominator of delta
    const unsigned nths; // # of threads

    Config(RandomMatrixType type, long dim, long nattempts,
           long da, long db, long nths):
        type(type), dim(dim), nattempts(nattempts),
        da(da), db(db), nths(nths) {}
};

static mutex cout_mtx; // mutex for std::cout

static Result avg_experiment(unsigned id, const Config &c)
{
    auto rmg = random_matrix_generator(c.type, c.dim);
    auto res = Result(c.dim);
    stringstream ss;
    for (auto i = id + 1; i <= c.nattempts; i += c.nths) {
        Mat<ZZ> A = rmg->gen();
        ZZ det2;
        ss << i << "/" << c.nattempts << ": ";
        string now{ss.str()};
        ss_reset(ss);
        ss << now << "starts (dim = " << c.dim << ")" << endl;
        th_log(id, ss, cout_mtx);
        auto start = chrono::system_clock::now();
        LLL(det2, A, c.da, c.db);
        auto end = chrono::system_clock::now();
        double elapsed = chrono::duration_cast<chrono::seconds>(end-start).count();
        ss << now << "elapsed time [s] = "
           << elapsed << endl;
        th_log(id, ss, cout_mtx);
        auto f = Factor(A[0], c.dim, det2);
        res.add(f, elapsed);
        ss << now << "f1 = " << f.factor1 << endl;
        th_log(id, ss, cout_mtx);
        ss << now << "f2 = " << f.factor2 << endl;
        th_log(id, ss, cout_mtx);
        ss << now << "f8 = " << f.factor8 << endl;
        th_log(id, ss, cout_mtx);
    }
    return res;
}

int main(int argc, char **argv)
{
    if (argc != 7) {
        cout << "usage: ./a.out type dim nattempts a b nthreads" << endl;
        exit(0);
    }
    string type_str = argv[1];
    RandomMatrixType type;
    if (type_str == random_matrix_type_str[KNAPSACK]) {
        type = KNAPSACK;
    } else if (type_str == random_matrix_type_str[AJTAI]) {
        type = AJTAI;
    } else {
        cout << "unknown type: " << type_str << endl;
        exit(1);
    }
    long d = strtol(argv[2], NULL, 10);
    long nattempts = strtol(argv[3], NULL, 10);
    long da = strtol(argv[4], NULL, 10);
    long db = strtol(argv[5], NULL, 10);
    if (!da || !db) {
        // default values
        da = 3;
        db = 4;
    }
    unsigned tmp = strtol(argv[6], NULL, 10);
    const unsigned nths =
        (1 <= tmp && tmp <= thread::hardware_concurrency())
        ? static_cast<unsigned>(tmp) : thread::hardware_concurrency();

    mt19937 rand_src(random_device{}());
    ZZ seed{static_cast<long>(rand_src())};
    SetSeed(seed);

    auto c = Config(type, d, nattempts, da, db, nths);
    vector<future<Result>> results;
    for (unsigned i = 0; i < nths; ++i)
        results.emplace_back(async(launch::async, avg_experiment, i, c));
    auto r = Result(d);
    for (unsigned i = 0; i < nths; ++i)
        r.merge(results[i].get());

    cout << endl;
    cout << "Settings:" << endl;
    cout << "type = " << random_matrix_type_str[c.type] << endl;
    cout << "nattempts = " << c.nattempts << endl;
    cout << "da = " << c.da << endl;
    cout << "db = " << c.db << endl;
    cout << "nths = " << c.nths << endl;
    cout << endl;
    cout << "Result (mean):" << endl;
    cout << "f1 = " << r.mean1() << endl;
    cout << "f2 = " << r.mean2() << endl;
    cout << "f8 = " << r.mean8() << endl;
    cout << "elapsed_time [s] = " << r.mean_elapsed() << endl;

    return 0;
}
