# Experiments on the Approximate Common Divisor (ACD) Problem
## License
MIT No Attribution.
See LICENSE.

## Dependency
To build programs, you need to install the GMP library and the NTL library.

Please modify Makefile as necessary to compile on your environment.
Note that if you use a shared library of libntl, you may want to change
the line starting with `LIBS := ...` in Makefile to

`LIBS := -lntl`

## How to Use
### avg
This program computes the average value of the length of the first vector
obtained by applying the LLL algorithm.
Specifically, it calculates the value of (||b\_1||/det(L)^{1/d})^{1/d},
where b\_1 is the first vector, det(L) is the determinant (volume) of
the lattice, and d is the dimension.
#### Usage
`./avg type dim nattempts a b nthreads`
* type: `ajtai` or `knapsack`
* dim: dimension of basis matrix (# of rows)
* nattempts: # of attempts
* a: numerator of delta in the LLL algorithm
* b: denominator of delta
* nthreads: # of threads
### sda
This program tries to solve the ACD problem by SDA approach using heuristics.
#### Usage
`./sda lambda plus_t nattempts a b nthreads`
* lambda: the security parameter of the scheme
* plus\_t: the integer added to dimension (dim = floor(gamma/eta) + plus\_t + 1)
* nattempts: # of attempts
* a: numerator of delta in the LLL algorithm
* b: denominator of delta
* nthreads: # of threads

## To Do List
* Add "helpful" comments.
* Add an orthogonal lattice approach.

## LLL on the Average: AVG
* Phong Q. Nguyen and Damien Stehlé. LLL on the average. In International algorithmic number theory symposium, pp. 238--256. Springer, 2006.

## ACD by Simultaneous Diophantine Approximation (SDA) Approach
* Steven D. Galbraith, Shishay W. Gebregiyorgis, and Sean Murphy. Algorithms for the approximate common divisor problem. LMS Journal of Computation and Mathematics, Vol. 19, No. A, p. 58--72, 2016.
* Marten Van Dijk, Craig Gentry, Shai Halevi, and Vinod Vaikuntanathan. Fully homomorphic encryption over the integers. In Annual International Conference on the Theory and Applications of Cryptographic Techniques, pp. 24-43. Springer, 2010.
