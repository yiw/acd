// SPDX-License-Identifier: MIT-0

#pragma once
#include <algorithm>
#include <vector>
#include <NTL/RR.h>

struct Result {
    const long dim;
    std::vector<NTL::RR> factor1, factor2, factor8;
    std::vector<double> elapsed;

    Result(long dim): dim(dim) {}
    virtual ~Result() {}
    virtual void add(const Factor &f, double elapsed) {
        factor1.emplace_back(f.factor1);
        factor2.emplace_back(f.factor2);
        factor8.emplace_back(f.factor8);
        this->elapsed.emplace_back(elapsed);
    }
    virtual void merge(const Result &r) {
        copy(r.factor1.begin(), r.factor1.end(), back_inserter(factor1));
        copy(r.factor2.begin(), r.factor2.end(), back_inserter(factor2));
        copy(r.factor8.begin(), r.factor8.end(), back_inserter(factor8));
        copy(r.elapsed.begin(), r.elapsed.end(), back_inserter(elapsed));
    }
    NTL::RR mean1() const {
        return std::accumulate(factor1.begin(), factor1.end(), NTL::to_RR(0)) /
            NTL::to_RR(factor1.size());
    }
    NTL::RR mean2() const {
        return std::accumulate(factor2.begin(), factor2.end(), NTL::to_RR(0)) /
            NTL::to_RR(factor2.size());
    }
    NTL::RR mean8() const {
        return std::accumulate(factor8.begin(), factor8.end(), NTL::to_RR(0)) /
            NTL::to_RR(factor8.size());
    }
    NTL::RR mean_elapsed() const {
        return std::accumulate(elapsed.begin(), elapsed.end(), NTL::to_RR(0)) /
            NTL::to_RR(elapsed.size());
    }
};
