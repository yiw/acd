// SPDX-License-Identifier: MIT-0

#pragma once
#include <NTL/ZZ.h>

class Acd {
protected:
    // Parameters
    long _lambda; // security parameter
    long _rho;    // bit-length of the error term
    long _eta;    // bit-length of the secret key $p$
    long _gamma;  // bit-length of the integers in the public key

    NTL::ZZ _p;   // secret key

private:
    NTL::ZZ _pow2_gamma_over_p;
    NTL::ZZ _pow2_rho;
    NTL::ZZ _pow2_rho_p1_m1;

    virtual NTL::ZZ gen_seckey();
    virtual void set_params(long lambda);
    void set_vars();

public:
    // If lambda < 2, it is 2.
    Acd(long lambda);
    virtual ~Acd() {}
    /*
     * Regenerate the seckey.
     * If lambda < 2, it is unchanged.
     */
    void reset(long lambda=0);
    /*
     * Return pq + r.
     * If exact_multiple = true, it returns pq.
     */
    NTL::ZZ sample(bool exact_multiple=false) const;
    long lambda() const { return _lambda; }
    long rho() const { return _rho; }
    long eta() const { return _eta; }
    long gamma() const { return _gamma; }
    NTL::ZZ p() const { return _p; }
};
