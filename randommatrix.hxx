// SPDX-License-Identifier: MIT-0

#pragma once
#include <memory>
#include <NTL/mat_ZZ.h>
#include <NTL/RR.h>

enum RandomMatrixType {
    KNAPSACK,
    AJTAI
};

const static char *random_matrix_type_str[] = {
    "knapsack",
    "ajtai"
};

class RandomMatrix {
protected:
    long _d; // dimension
public:
    RandomMatrix(long d): _d(d) {}
    virtual ~RandomMatrix() {}
    virtual NTL::Mat<NTL::ZZ> gen() = 0;
};

class RandomKnapsackMatrix: public RandomMatrix {
public:
    RandomKnapsackMatrix(long d): RandomMatrix(d) { }
    NTL::Mat<NTL::ZZ> gen() override {
        NTL::ZZ B = NTL::power2_ZZ(100 * _d);
        NTL::Mat<NTL::ZZ> A;
        A.SetDims(_d, _d + 1);
        for (auto i = 1; i <= _d; ++i)
            A[i - 1][i] = 1;
        for (auto i = 0; i < _d; ++i)
            A[i][0] = NTL::RandomBnd(2 * B + 1) - B;
        return A;
    }
};

class RandomAjtaiMatrix: public RandomMatrix {
public:
    RandomAjtaiMatrix(long d): RandomMatrix(d) { }
    NTL::Mat<NTL::ZZ> gen() override {
        NTL::RR a{1.2}; // TODO: Make it customizable.
        NTL::Mat<NTL::ZZ> A;
        A.SetDims(_d, _d);
        for (auto i = 0; i < _d; ++i) {
            auto e = pow(NTL::to_RR(2 * _d - i), a);
            auto b = FloorToZZ(pow(NTL::to_RR(2), e));
            A[i][i] = b;
            for (auto j = i + 1; j < _d; ++j) {
                NTL::RR b2 = NTL::to_RR(b) / 2.0;
                NTL::ZZ l = NTL::CeilToZZ(-b2);
                NTL::ZZ r = NTL::FloorToZZ(b2);
                A[j][i] = NTL::RandomBnd(r - l + 1) + l;
            }
        }
        return A;
    }
};

static std::unique_ptr<RandomMatrix> random_matrix_generator(RandomMatrixType type, long d)
{
    switch (type) {
    case KNAPSACK:
        return std::unique_ptr<RandomMatrix>(new RandomKnapsackMatrix(d));
    case AJTAI:
        return std::unique_ptr<RandomMatrix>(new RandomAjtaiMatrix(d));
    }
}
