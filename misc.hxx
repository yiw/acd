// SPDX-License-Identifier: MIT-0

#pragma once
#include <iostream>
#include <sstream>
#include <mutex>

static inline void ss_reset(std::stringstream &ss)
{
    ss.str("");
    ss.clear(std::stringstream::goodbit);
}

static inline void th_log(unsigned id, std::stringstream &ss, std::mutex &mtx)
{
    {
        std::lock_guard<std::mutex> lock(mtx);
        std::cout << "id " << id << ": " << ss.str();
    }
    ss_reset(ss);
}
